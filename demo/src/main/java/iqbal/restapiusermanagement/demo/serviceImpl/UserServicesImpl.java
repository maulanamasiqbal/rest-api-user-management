package iqbal.restapiusermanagement.demo.serviceImpl;

import iqbal.restapiusermanagement.demo.dto.UserDto;
import iqbal.restapiusermanagement.demo.entity.User;
import iqbal.restapiusermanagement.demo.exception.EmailAlreadyExistsException;
import iqbal.restapiusermanagement.demo.exception.ResourceNotFoundException;
import iqbal.restapiusermanagement.demo.mapper.AutoUserMapper;
import iqbal.restapiusermanagement.demo.mapper.UserMapper;
import iqbal.restapiusermanagement.demo.repository.UserRepository;
import iqbal.restapiusermanagement.demo.services.UserServices;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class UserServicesImpl implements UserServices {

    private UserRepository userRepository;

    private ModelMapper modelMapper;
    @Override
    public UserDto createUser(UserDto userDto) {

        //convert userDto into User JPA Entity

//        User user = UserMapper.mapToUser(userDto);

//        User user = modelMapper.map(userDto, User.class);

        Optional<User> optionalUser = userRepository.findByEmail(userDto.getEmail());

        if (optionalUser.isPresent()){
            throw new EmailAlreadyExistsException("Email Already Exists for User");
        }

        User user = AutoUserMapper.MAPPER.mapToUser(userDto);

        User saveUser = userRepository.save(user);

        // Convert User JPA Entity into userDto

//        UserDto savedUserDto = UserMapper.mapToUserDto(saveUser);

//        UserDto savedUserDto = modelMapper.map(saveUser, UserDto.class);

        UserDto savedUserDto = AutoUserMapper.MAPPER.mapToUserDto(saveUser);

        return savedUserDto;
    }

    @Override
    public UserDto getUserById(Long userId) {
        User byId = userRepository.findById(userId).orElseThrow(
                () -> new ResourceNotFoundException("User", "userId", userId)
        );
//        User user = byId.get();
//        return UserMapper.mapToUserDto(user);

//        return modelMapper.map(user, UserDto.class);

        return AutoUserMapper.MAPPER.mapToUserDto(byId);
    }

    @Override
    public List<UserDto> getUserAll() {
        List<User> all = userRepository.findAll();

//        return all.stream().map(UserMapper::mapToUserDto)
//                .collect(Collectors.toList());

//        return all.stream().map((user) -> modelMapper.map(user, UserDto.class))
//                .collect(Collectors.toList());

        return all.stream().map((user) -> AutoUserMapper.MAPPER.mapToUserDto(user))
                .collect(Collectors.toList());
    }

    @Override
    public UserDto updateUser(UserDto userDto) {
        User existingUser = userRepository.findById(userDto.getId()).orElseThrow(
                () -> new ResourceNotFoundException("User", "userId", userDto.getId())
        );
        existingUser.setFirstName(userDto.getFirstName());
        existingUser.setLastName(userDto.getLastName());
        existingUser.setEmail(userDto.getEmail());

        //update
        User updateUser = userRepository.save(existingUser);

//        return UserMapper.mapToUserDto(updateUser);

//        return modelMapper.map(updateUser, UserDto.class);

        return AutoUserMapper.MAPPER.mapToUserDto(updateUser);
    }

    @Override
    public void deleteUser(Long userId) {
        User existingUser = userRepository.findById(userId).orElseThrow(
                () -> new ResourceNotFoundException("User", "userId", userId)
        );
        userRepository.deleteById(existingUser.getId());
    }
}
