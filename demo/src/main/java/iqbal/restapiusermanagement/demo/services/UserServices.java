package iqbal.restapiusermanagement.demo.services;

import iqbal.restapiusermanagement.demo.dto.UserDto;

import java.util.List;

public interface UserServices {

    UserDto createUser(UserDto userDto);

    UserDto getUserById(Long userId);

    List<UserDto> getUserAll();

    UserDto updateUser(UserDto userDto);

    void deleteUser(Long userId);
}
