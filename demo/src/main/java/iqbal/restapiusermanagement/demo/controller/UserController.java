package iqbal.restapiusermanagement.demo.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import iqbal.restapiusermanagement.demo.dto.UserDto;
import iqbal.restapiusermanagement.demo.entity.User;
import iqbal.restapiusermanagement.demo.services.UserServices;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Tag(
        name = "CRUD REST API for Users Resource",
        description = "CRUD REST APIs - Create User, Update User, Delete User, Get User, Get All User"
)
@RestController
@AllArgsConstructor
@RequestMapping("/api/users")
public class UserController {

    private UserServices userServices;

    @Operation(
            summary = "Create User REST API",
            description = "Create User REST API is used save user in a database"
    )
    @ApiResponse(
            responseCode = "201",
            description = "HTTP Status 201 CREATE"
    )
    //build create User REST API
    @PostMapping("/")
    public ResponseEntity<UserDto> createUser(@Valid @RequestBody UserDto userDto){
        UserDto createUser = userServices.createUser(userDto);
        return new ResponseEntity<>(createUser, HttpStatus.CREATED);
    }

    @Operation(
            summary = "GET User By ID REST API",
            description = "GET User By ID REST API is used to get a single user from the database"
    )
    @ApiResponse(
            responseCode = "200",
            description = "HTTP Status 200 SUCCESS"
    )
    //build get user by id User REST API
    @GetMapping("{id}")
    public ResponseEntity<UserDto> getUserById(@PathVariable("id") Long userId){
        UserDto userById = userServices.getUserById(userId);
        return new ResponseEntity<>(userById, HttpStatus.OK);
    }

    @Operation(
            summary = "GET All User REST API",
            description = "GET All User REST API is used to get a all the users from the database"
    )
    @ApiResponse(
            responseCode = "200",
            description = "HTTP Status 200 SUCCESS"
    )
    //build get all user User REST API
    @GetMapping("/")
    public ResponseEntity<List<UserDto>> getUserAll(){
        List<UserDto> userAll = userServices.getUserAll();
        return new ResponseEntity<>(userAll, HttpStatus.OK);
    }

    @Operation(
            summary = "UPDATE User REST API",
            description = "Update User REST API is used to update a particular user in the database"
    )
    @ApiResponse(
            responseCode = "200",
            description = "HTTP Status 200 SUCCESS"
    )
    //build update User REST API
    @PutMapping("{id}")
    public ResponseEntity<UserDto> updateUser(@PathVariable("id") Long userId,
                                           @RequestBody @Valid UserDto userDto){
        userDto.setId(userId);
        UserDto updated = userServices.updateUser(userDto);
        return new ResponseEntity<>(updated, HttpStatus.OK);
    }

    @Operation(
            summary = "DELETE User REST API",
            description = "DELETE User REST API is used to delete a particular user from the database"
    )
    @ApiResponse(
            responseCode = "200",
            description = "HTTP Status 200 SUCCESS"
    )
    //build delete User REST API
    @DeleteMapping("{id}")
    public ResponseEntity<String> deleteUser(@PathVariable("id") Long userId){
        userServices.deleteUser(userId);
        return new ResponseEntity<>("User successfully deleted!", HttpStatus.OK);
    }
}
